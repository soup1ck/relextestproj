package com.relex.relextest.cache;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
@Component
@Slf4j
public class FileCache {

    private String filePath;

    public String checkSumOfFile(){
        try (InputStream is = Files.newInputStream(Paths.get(this.filePath))) {
            return DigestUtils.md5Hex(is);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return "Контрольная сумма файла не посчитана";
    }
}
