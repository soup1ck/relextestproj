package com.relex.relextest.service.fileservice;

import com.relex.relextest.cache.FileCache;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@Getter
@RequiredArgsConstructor
@Slf4j
public class FileServiceImpl implements FileService {

    private final FileCache fileCache;

    @Override
    @Cacheable(value = "listOfNumbers", key = "@fileCache.checkSumOfFile()")
    public List<Integer> getListOfNumbers(String filePath) {
        List<Integer> listOfNumbers = new ArrayList<>();
        try (FileReader reader = new FileReader(filePath);
             BufferedReader bufferedReader = new BufferedReader((reader))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                listOfNumbers.add(Integer.valueOf(line));
            }
            return listOfNumbers;
        } catch (IOException e) {
            log.error(e.getMessage());
            return listOfNumbers;
        }
    }

    public List<Integer> sortListOfNumbers(List<Integer> integerList) {
        return integerList.parallelStream()
                .sorted()
                .toList();
    }
}

