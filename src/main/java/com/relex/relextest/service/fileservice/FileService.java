package com.relex.relextest.service.fileservice;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public interface FileService {

    List<Integer> getListOfNumbers(String filePath) throws IOException;
}
