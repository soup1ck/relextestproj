package com.relex.relextest.service.statisticsservice;

import com.relex.relextest.data.request.FileDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StatisticService {

    Integer getMaxValue(FileDto fileDto);
    Integer getMinValue(FileDto fileDto);
    Number getMedian(FileDto fileDto);
    Double getAverageSum(FileDto fileDto);
    List<List<Integer>> getLongestSeqIncreases(FileDto fileDto);
    List<List<Integer>> getLongestSeqDecreases(FileDto fileDto);
}
