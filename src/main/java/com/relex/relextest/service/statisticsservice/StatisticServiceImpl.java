package com.relex.relextest.service.statisticsservice;

import com.relex.relextest.data.request.FileDto;
import com.relex.relextest.service.fileservice.FileServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class StatisticServiceImpl implements StatisticService {

    private final FileServiceImpl fileService;

    @Override
    @Cacheable(value = "maxValue", key = "@fileCache.checkSumOfFile()")
    public Integer getMaxValue(FileDto fileDto) {
        List<Integer> listOfNumbers = fileService.getListOfNumbers(fileDto.getFilePath());
        return listOfNumbers.stream()
                .mapToInt(v -> v)
                .max()
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    @Cacheable(value = "minValue", key = "@fileCache.checkSumOfFile()")
    public Integer getMinValue(FileDto fileDto) {
        List<Integer> listOfNumbers = fileService.getListOfNumbers(fileDto.getFilePath());
        return listOfNumbers.stream()
                .mapToInt(v -> v)
                .min()
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    @Cacheable(value = "averageSum", key = "@fileCache.checkSumOfFile()")
    public Double getAverageSum(FileDto fileDto) {
        List<Integer> listOfNumbers = fileService.getListOfNumbers(fileDto.getFilePath());
        return listOfNumbers.stream()
                .mapToInt(v -> v)
                .average()
                .orElse(0);
    }

    @Override
    @Cacheable(value = "median", key = "@fileCache.checkSumOfFile()")
    public Number getMedian(FileDto fileDto) {
        List<Integer> listOfNumbers = fileService.getListOfNumbers(fileDto.getFilePath());
        List<Integer> sortedList = fileService.sortListOfNumbers(listOfNumbers);
        if (sortedList.size() / 2 != 0) {
            return sortedList.get(sortedList.size() / 2);
        }
        return (sortedList.get(sortedList.size() - 1) + sortedList.size()) / 2;
    }

    @Override
    @Cacheable(value = "lon_seq_inq", key = "@fileCache.checkSumOfFile()")
    public List<List<Integer>> getLongestSeqIncreases(FileDto fileDto) {
        List<Integer> integerList = fileService.getListOfNumbers(fileDto.getFilePath());
        List<Integer> bufferSeq = new ArrayList<>();
        List<List<Integer>> seq = new ArrayList<>();
        int count = 0;
        int max = 0;
        bufferSeq.add(integerList.get(0));
        for (int i = 1; i < integerList.size(); i++) {
            if (integerList.get(i) > bufferSeq.get(count)) {
                bufferSeq.add(integerList.get(i));
                count++;
            } else {
                if (bufferSeq.size() >= max) {
                    seq.clear();
                    max = bufferSeq.size();
                    seq.add(bufferSeq);
                }
                count = 0;
                bufferSeq = new ArrayList<>();
                bufferSeq.add(integerList.get(i));
            }
            if (bufferSeq.size() > max) {
                seq.clear();
                seq.add(bufferSeq);
            }
            if (bufferSeq.size() == max) {
                seq.add(bufferSeq);
            }
        }
        return seq;
    }

    @Override
    @Cacheable(value = "longest_seq_deq", key = "@fileCache.checkSumOfFile()")
    public List<List<Integer>> getLongestSeqDecreases(FileDto fileDto) {
        List<Integer> integerList = fileService.getListOfNumbers(fileDto.getFilePath());
        List<Integer> bufferSeq = new ArrayList<>();
        List<List<Integer>> seq = new ArrayList<>();
        int count = 0;
        int max = 0;
        bufferSeq.add(integerList.get(0));
        for (int i = 1; i < integerList.size(); i++) {
            if (integerList.get(i) < bufferSeq.get(count)) {
                bufferSeq.add(integerList.get(i));
                count++;
            } else {
                if (bufferSeq.size() >= max) {
                    seq.clear();
                    max = bufferSeq.size();
                    seq.add(bufferSeq);
                }
                count = 0;
                bufferSeq = new ArrayList<>();
                bufferSeq.add(integerList.get(i));
            }
            if (bufferSeq.size() > max) {
                seq.clear();
                seq.add(bufferSeq);
            }
            if (bufferSeq.size() == max) {
                seq.add(bufferSeq);
            }
        }
        return seq;
    }

    public Object selectTypeOfOperation(FileDto fileDto) {
        switch (fileDto.getOperation()) {
            case "get_max_value" -> {
                return getMaxValue(fileDto);
            }
            case "get_min_value" -> {
                return getMinValue(fileDto);
            }
            case "get_median" -> {
                return getMedian(fileDto);
            }
            case "get_average_sum" -> {
                return getAverageSum(fileDto);
            }
            case "get_longest_seq_increases" -> {
                return getLongestSeqIncreases(fileDto);
            }
            case "get_longest_seq_decreases" -> {
                return getLongestSeqDecreases(fileDto);
            }
            default -> {
                return "operation not found";
            }
        }
    }
}
