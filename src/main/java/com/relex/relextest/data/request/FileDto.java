package com.relex.relextest.data.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileDto {

    @JsonProperty("file_path")
    private String filePath;

    @JsonProperty("operation")
    private String operation;
}
