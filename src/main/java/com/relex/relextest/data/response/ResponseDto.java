package com.relex.relextest.data.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Builder;

import java.util.List;

@Builder
@JacksonXmlRootElement(localName = "answer")
public class ResponseDto {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("max_value")
    private Integer maxValue;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("min_value")
    private Integer minValue;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("median")
    private Number median;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("average_sum")
    private Double averageSum;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("longest_seq_increases")
    @JacksonXmlElementWrapper(localName = "longest_seq_increases")
    @JacksonXmlProperty(localName = "itemIn")
    private List<List<Integer>> longestSeqIncreases;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("longest_seq_decreases")
    @JacksonXmlElementWrapper(localName = "longest_seq_decreases")
    @JacksonXmlProperty(localName = "itemDe")
    private List<List<Integer>> longestSeqDecreases;
}
