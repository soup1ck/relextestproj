package com.relex.relextest.controller;

import com.relex.relextest.cache.FileCache;
import com.relex.relextest.data.request.FileDto;
import com.relex.relextest.data.response.ResponseDto;
import com.relex.relextest.service.statisticsservice.StatisticServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping()
public class FileController {

    private final StatisticServiceImpl staticService;
    private final FileCache fileCache;

    @PostMapping("/addFile")
    public ResponseEntity<?> addFile(@RequestBody FileDto fileDto){
        fileCache.setFilePath(fileDto.getFilePath());
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/max-value",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ResponseDto> getMaxOfSeq(@RequestBody FileDto fileDto) {
        ResponseDto responseDto = ResponseDto.builder()
                .maxValue(staticService.getMaxValue(fileDto))
                .build();
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @GetMapping(value = "/min-value",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ResponseDto> getMinOfSeq(@RequestBody FileDto fileDto) {
        ResponseDto responseDto = ResponseDto.builder().
                minValue(staticService.getMinValue(fileDto))
                .build();
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @GetMapping(value = "/median",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ResponseDto> getMedianOfSeq(@RequestBody FileDto fileDto) {
        ResponseDto responseDto = ResponseDto.builder()
                .median(staticService.getMedian(fileDto))
                .build();
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @GetMapping(value = "/average-sum",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ResponseDto> getAverageSumOfSeq(@RequestBody FileDto fileDto) {
        ResponseDto responseDto = ResponseDto.builder().
                averageSum(staticService.getAverageSum(fileDto))
                .build();
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @GetMapping(value = "/longest-seq-inq",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ResponseDto> getLongestSeqIncreases(@RequestBody FileDto fileDto) {
        ResponseDto responseDto = ResponseDto.builder()
                .longestSeqIncreases(staticService
                        .getLongestSeqIncreases(fileDto))
                .build();
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @GetMapping(value = "/longest-seq-deq",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ResponseDto> getLongestSeqDecreases(@RequestBody FileDto fileDto) {
        ResponseDto responseDto = ResponseDto.builder()
                .longestSeqIncreases(staticService
                        .getLongestSeqDecreases(fileDto))
                .build();
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }
}
